/**
 * orders_item collection
 * used for realtime kitchen line ticket printing
 * {
 *  menuitem_name
 *  notes (like no celantro, no spicy...)
 *  tableid
 * }
 */
Orders_Print=new Mongo.Collection('orders_print');


/**
 * reciept collection
 * {
 *  items:[{
 *      name,
 *      quantity,
 *      price
 *  }]
 *
 *  total
 *  cartseqnumber
 *  date
 * }
 */
RecieptPrint=new Mongo.Collection('reciept_print');


/**
 * kitchenline_item collection
 * used for realtime kitchen line ticket printing
 * {
 *  menuitem_name
 *  notes (like no celantro, no spicy...)
 *  tableid
 *  itemseqnumber
 *  cartseqnumber
 * }
 */
KitchenLine_Print=new Mongo.Collection('kitchenline_print');

