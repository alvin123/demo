//printing methods
Meteor.methods({
    /**
     *  print the orders
     *  input{
     *      tableid,
     *      cartseqnumber,
     *      orders[{
     *          name,price,gst,pst
     *      }]
     *
     *  }
     */
    print_table_orders: function (input) {

        RecieptPrint.insert({
            tableid: input.tableid,
            orders: input.orders,
            cartseqnumber:input.cartseqnumber.toFixed(0)
        });
    },


    /**
     * @param reciept{
         seqnumber,
         subtotal,
         gsttotal,
         psttotal,
         total,
         tableid,
         orders:[{
           name,price,gst,pst
         }]
     * }
     */
    print_reciept: function (reciept) {
        if(reciept.seqnumber==undefined||
           reciept.cartseqnumber==undefined||
           reciept.gsttotal==undefined||
           reciept.psttotal==undefined||
           reciept.subtotal==undefined||
           reciept.total==undefined||
           reciept.tableid==undefined||
           reciept.orders==undefined
           )
            throw new Meteor.Error('can not print reciept - illegal reciept formate');


        reciept.seqnumber=reciept.seqnumber.toFixed(0);
        reciept.cartseqnumber=reciept.cartseqnumber.toFixed(0);

        reciept.time=new Date();
        RecieptPrint.insert(reciept);


    },

    /**
     * print kitchenline ticket
     * input{
     *  menuitem_name,
     *  notes,
     *  tableid,
     *  cartseqnumber,
     *  itemseqnumber
     * }
     */
    print_kitchenline:function(input){
        KitchenLine_Print.insert(input);
    },



});

//DO NOT CHANGE!
//printing method callbacks used by java autoprint client
Meteor.methods({

    kitchenline_print_printed:function(id){
        KitchenLine_Print.remove(id);
    },
    orderslist_print_printed:function(id){
        Orders_Print.remove(id);
    },
    reciept_print_printed: function (id) {
        RecieptPrint.remove(id);
    }



});

