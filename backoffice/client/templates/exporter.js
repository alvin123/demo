CSVExporter = {
	exportAll: function() {
		var self = this;
		Meteor.call("exportAll", function(error, data) {
 
			if ( error ) {
				alert(error); 
				return false;
			}
			
			var csv = Papa.unparse(data);
			self._downloadCSV(csv,"DailySales.csv");
		});
	},

	exportMembership:function(){
		var self = this;
		Meteor.call("exportMembership", function(error,data){
			if(error){
				alert(error);
				return false;
			}

			var csv = Papa.unparse(data);
			self._downloadCSV(csv,"Membership.csv");
		});
	},
 
	_downloadCSV: function(csv,filename) {
		var blob = new Blob([csv]);
		var a = window.document.createElement("a");
	    a.href = window.URL.createObjectURL(blob, {type: "text/plain"});
	    a.download = filename;
	    document.body.appendChild(a);
	    a.click();
	    document.body.removeChild(a);
	}
}

/*
MemberExporter = {
	exportAll: function() {
		var self = this;
		Meteor.call("exportAll", function(error, data) {
 
			if ( error ) {
				alert(error); 
				return false;
			}
			
			var csv = Papa.unparse(data);
			self._downloadCSV(csv);
		});
	},
 
	_downloadCSV: function(csv) {
		var blob = new Blob([csv]);
		var a = window.document.createElement("a");
	    a.href = window.URL.createObjectURL(blob, {type: "text/plain"});
	    a.download = "MemberList.csv";
	    document.body.appendChild(a);
	    a.click();
	    document.body.removeChild(a);
	}
}
*/