Meteor.methods({
    keycheck: function(thiskey) {
        var currentKey = Keykey.findOne({_id:"sammykey"});
        // console.log(currentKey.keykey);
        // console.log(thiskey);
        if(parseInt(thiskey) == parseInt(currentKey.keykey)){
            return Book.findOne({"name":"today"});
        }
        else{
            return false;
        }
    },




        changePassword_backoffice:function(old, newPass, newCheck, thiskey) { 
        var oldKey = Keykey.findOne({_id:"sammykey"});
        var result;
        Meteor.call('check_key',thiskey, function(err, res){
            if(res){
                if(old == oldKey.keykey){
                    if(newPass == newCheck){
                        Keykey.update({_id:"sammykey"},{$set:{keykey:newPass}});
                        alert('密码修改成功');
                    }
                    else{
                        alert('请输入相同新密码');
                    }
                }
                else{
                    alert('旧密码错误');                                    
                }
            }
            else{
                alert('管理员密码错误');
            }
        });

        return result;
    },

    systemreset: function(){

        Cart.remove({});
        Receipts.remove({});
        Book.remove({});

        OrderSeq.remove({});
        ItemSeq.remove({});
        ReceiptSeq.remove({});

        Book.insert({"name":"today", "sales":0, "cash":0, "card":0, "tips":0});

        OrderSeq.insert({_id:"userid", increment: parseInt(0)});
        ItemSeq.insert({_id:"userid", increment: parseInt(0)});
        ReceiptSeq.insert({_id:"userid", increment: parseInt(0)});

        return "系统复位完成";
    },
    exportAll: function(){
        var fields = [
            "小票号",
            "桌号",
            "消费",
            "GST",
            "PST",
            "合计",
            "现金",
            "卡",
            "点餐细目"
        ];

        var data = [];

        var receipts = Receipts.find().fetch();
        _.each(receipts, function(r){
            if (r.paid) {
                data.push([
                    r.seqnumber,
                    r.tableid,
                    r.subtotal,
                    r.gst,
                    r.pst,
                    r.amount,
                    r.cashpay,
                    r.cardpay,
                    r.ordernames
                ]);
            }
        });

        return{fields: fields, data: data};

    },

    exportMembership: function(){
        var fields = [
            "MemberID",
            "CreatTime",
            "Firstname",
            "LastName",
            "Phone",
            "Email"
        ];

        var data = [];
        var history = [];

        var membership = Membership.find().fetch();
        _.each(membership, function(r){

            data.push([
                r.memberid,
                r.member_created_time,
                r.memberfirstname,
                r.memberlastname,
                r.memberphone,
                r.memberemail
                ]);
        });
        return{fields: fields, data: data};
    }
});




/*

Meteor.methods({
    test_sales_total_data:function(){
        Transactions.insert({
            total:'2.2',
            paymethod:'cash',
            time:moment().startOf('month').toDate()
        });
        Transactions.insert({
            total:'1.1',
            paymethod:'card',
            time:moment().startOf('year').toDate()
        });
        Transactions.insert({
            total:'3.3',
            paymethod:'card',
            time:moment().startOf('day').toDate()
        });
    },
    sales_total: function (range) {
        var map = function() {
            if(this.paymethod=='card')
                emit('TransactionTotal_CARD',this.total);
            else
                emit('TransactionTotal_CASH',this.total);
            emit('TransactionTotal',this.total);
        }

        var reduce = function(key, totals) {
            var count = '0';

            for (var i=0;i<totals.length;i++) {
                count = +count+ +totals[i];
                count=parseFloat(count).toFixed(2);
            }

            return count;
        }
        var validRanges=['day','week','month','year'];
        if(validRanges.indexOf(range)<0)
            throw new Meteor.Error('MapReduce sales_total failed: invalid date range:'+range);

        var dateStart=moment().startOf(range).toDate();
        console.log('====asd');
        console.log(dateStart);
        // keep in mind that executing the mapReduce function will override every time the collection Tags
        var result = Transactions.mapReduce(map, reduce, {query: {time:{$gte:dateStart}}, out: "MapReduceResults"});

        // now return all the tags, ordered by usage
        // as an alternative solution you can also publish the collection Tags and use this one at the client side
        return {
            total:MapReduceResults.findOne({_id:'TransactionTotal'}),
            cash:MapReduceResults.findOne({_id:'TransactionTotal_CASH'}),
            card:MapReduceResults.findOne({_id:'TransactionTotal_CARD'})
        };
    }

});
*/