

// -------------------------------------------------------------

// used by narasace
Receipts = new Mongo.Collection('receipts');

/*
 * receipts collection
 * {
 *  cartid
 *  cartseq
 *  time
 *  orders:[{
 *    car.orders.name
 *    cart.orders.seqnumber
 *    cart.orders.price / split_divider
 *    cart.orders.gst / split_divider
 *    cart.orders.pst / split_divider
 *  }]
 *  subtotal
 *  gst
 *  pst
 *  amount
 *  paid
 *  tips
 *  cash: boolean
 *  card: boolean
 *  card_receipt_digits
 *  paid: boolean
 * }
 */

// The book for bookkeeping
Book = new Mongo.Collection('book');

// -------------------------------------------------------------

/**
 * table collection
 * {
 *  name (str)
 *  order number (int)
 *  bing (boolean)
 * }
 */
Table = new Mongo.Collection('table');

/*
 * category collection
 * {
 *  name (str)
 *  pic_id (uri)
 * }
 *
 * */
Category = new Mongo.Collection("category");

/*
 * menuitem collection
 * {
 *  category (_id)
 *  name (str)
 *  price (float)
 *  pic_id (uri)
 *  item_id (str)
 *  ingred
 * }
 *
 * */
MenuItem = new Mongo.Collection("menuitem");

/*
 * cart collection
 * {
 *  tableid
 *  date
 *  order_id (int)
 *  orders:[{
 *      itemid
 *      name
 *      pic_id
 *      ingred
 *      status (pending/waiting/done/paid)   pending for customer just ordered. waiting when waitress comfirmed the orders
 *  }]
 * }
 *
 * */
Cart = new Mongo.Collection("cart");

/*
 *
 * orders collection
 * {
 *
 *
 * }
 * */

Orders = new Mongo.Collection("orders");

/*
 * transactions collection
 * {
 *  all the fields in cart
 *  paymethod (card/cash)
 * }
 * */

Transactions = new Mongo.Collection("transactions");

/*
 * membership collection
 * {
 *   name
 *   phone
 *   email
 *   id
 *   type
 * }
 *
 **/

 Membership = new Mongo.Collection("membership");

/*
 * sequence collection for carts and ordered items
 * {
 * _id: "userid",
 * seq: (int)
 *
 *
 * */
 Ingred = new Mongo.Collection("ingred");

 OrderSeq = new Mongo.Collection("order_counter");

 ItemSeq = new Mongo.Collection("item_counter");

 ReceiptSeq = new Mongo.Collection("receipt_counter");

 Keykey = new Mongo.Collection('keykey');




if(Meteor.isServer){
    /**
     * initialization
     */

    Transactions.remove({});
    Table.remove({});
    Category.remove({});
    MenuItem.remove({});
    Keykey.remove({});
    Cart.remove({});
    Receipts.remove({});
    Book.remove({});
    Membership.remove({});
    OrderSeq.remove({});
    ItemSeq.remove({});
    Ingred.remove({});
    ReceiptSeq.remove({});

    
    Keykey.insert({_id:"sammykey", "keykey": 68752299 });
    Book.insert({"name":"today", "sales":0, "cash":0, "card":0, "tips":0});

    OrderSeq.insert({_id:"userid", increment: parseInt(0)});
    ItemSeq.insert({_id:"userid", increment: parseInt(0)});
    ReceiptSeq.insert({_id:"userid", increment: parseInt(0)});

    
    
    Table.insert({name:"red1", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"red2", order_number:"", inuse:false, bing:false, computeron:false});
  

    Table.insert({name:"blue1", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"blue2", order_number:"", inuse:false, bing:false, computeron:false});


    Table.insert({name:"yellow1", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"yellow2", order_number:"", inuse:false, bing:false, computeron:false});


    Table.insert({name:"green1", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"green2", order_number:"", inuse:false, bing:false, computeron:false});


    Table.insert({name:"purple1", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"purple2", order_number:"", inuse:false, bing:false, computeron:false});

    Table.insert({name:"s11", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"s12", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"s13", order_number:"", inuse:false, bing:false, computeron:false});



    Table.insert({name:"t1", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"t2", order_number:"", inuse:false, bing:false, computeron:false});

    Table.insert({name:"m1", order_number:"", inuse:false, bing:false, computeron:false});

    Table.insert({name:"cb1", order_number:"", inuse:false, bing:false, computeron:false});

    Table.insert({name:"vred1", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"vred2", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"vred3", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"vred4", order_number:"", inuse:false, bing:false, computeron:false});
    Table.insert({name:"vred5", order_number:"", inuse:false, bing:false, computeron:false});

    Table.insert({name:"c1", order_number:"", inuse:false, bing:false, computeron:false});

    Ingred.insert({_id:"Sugar", name: "Sugar", quantity: 100});
    Ingred.insert({_id:"Milk", name: "Milk", quantity: 100});
    Ingred.insert({_id:"Salt", name: "Salt", quantity: 100});
    Ingred.insert({_id:"Honey", name: "Honey", quantity: 100});


    catid =Category.insert({seqnumber:"1a", name: "Single Item", pic_id:"dk.png", quantity:0});
    MenuItem.insert({category: catid, seqnumber:'1', name: "Pops", price: 2.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 100, gst:0, pst:0, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'2', name: "Water", price: 2.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 100, gst:0, pst:0, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'3', name: "San Pellegrino", price: 2.50, pic_id: "dk.png", itemid:"", ingred: "", quantity: 100, gst:0, pst:0, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'4', name: "Starbucks Frappucino", price: 4.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 100, gst:0, pst:0, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'5', name: "Vitamin Water", price: 3.50, pic_id: "dk.png", itemid:"", ingred: "", quantity: 100, gst:0, pst:0, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'6', name: "Gatorade", price: 3.50, pic_id: "dk.png", itemid:"", ingred: "", quantity: 100, gst:0, pst:0, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'7', name: "Milk Coffee", price: 3.50, pic_id: "dk.png", itemid:"", ingred: "", quantity: 100, gst:0, pst:0, printbypass:true});
   
       var catid = Category.insert({seqnumber:"1b", name: "Item with model", pic_id: "dk.png", quantity:0});
    MenuItem.insert({category: catid, seqnumber:'51', name: "Soda(SR)", price: 5.00, pic_id: "dk.png", SR:1, gst:0.05, pst:0});
    MenuItem.insert({category: catid, seqnumber:'52', name: "Noodle(AO With price Change)", price: 5.00, pic_id: "dk.png", SR:2, gst:0.05, pst:0});
    MenuItem.insert({category: catid, seqnumber:'53', name: "Soda(Option that manipulate name)", price: 5.00, pic_id: "dk.png", SR:3, gst:0.05, pst:0});
    MenuItem.insert({category: catid, seqnumber:'54', name: "Item", price: 5.00, pic_id: "dk.png", SR:4, gst:0.05, pst:0});



    
    catid =Category.insert({seqnumber:"1c", name: "Inventory", pic_id:"dk.png", quantity:0});
    MenuItem.insert({category: catid, seqnumber:'101', name: "荔枝烏龍奶蓋(大)", eng_name:'Lychee Oolong Cheese Lattea', gst: 0.05, pst:0.07,  SR:5 ,hot: 0.50, price: 5.25, pic_id: "dk.png", ing1:-3, ing2:-2, ing3:-1, ing4:-1});
    MenuItem.insert({category: catid, seqnumber:'102', name: "桂花烏龍奶蓋(大)", eng_name:'Oolong Cheese Lattea', gst: 0.05, pst:0.07,  SR:5 ,hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:0, ing2:-1, ing3:0, ing4:-1});
    MenuItem.insert({category: catid, seqnumber:'103', name: "白桃烏龍奶蓋(大)", eng_name:'Peach Oolong Cheese Lattea', gst: 0.05, pst:0.07,  SR:5 ,hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:-1, ing2:-2, ing3:-1, ing4:-1});
    MenuItem.insert({category: catid, seqnumber:'104', name: "玫瑰紅茶奶蓋(大)", eng_name:'Rose Cheese Lattea', gst: 0.05, pst:0.07,  SR:5 ,hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:-1, ing2:0, ing3:0, ing4:-1});
    MenuItem.insert({category: catid, seqnumber:'105', name: "茉莉紅茶奶蓋(大)", eng_name:'Jasmin Cheese Lattea', gst: 0.05, pst:0.07,  SR:5 ,hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:-3, ing2:-1, ing3:-1, ing4:-1});
    MenuItem.insert({category: catid, seqnumber:'106', name: "玄米綠茶奶蓋(大)", eng_name:'Brown Rice Cheese Lattea', SR:5, hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:-1, ing2:-1, ing3:0, ing4:-1});
    MenuItem.insert({category: catid, seqnumber:'107', name: "高山鳥龍奶蓋(大)", eng_name:'Oolong tea lattea', gst: 0.05, pst:0.07,  SR:5 ,hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:-1, ing2:-1, ing3:0, ing4:-3});
    MenuItem.insert({category: catid, seqnumber:'108', name: "錫蘭紅茶奶蓋(大)", eng_name:'BBlack tea lattea', gst: 0.05, pst:0.07,  SR:5 ,hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:-2, ing2:0, ing3:0, ing4:-1});
    MenuItem.insert({category: catid, seqnumber:'109', name: "伯爵奶蓋(大)", eng_name:'Earl grey tea lattea', gst: 0.05, pst:0.07,  SR:5 ,hot:0.5, price: 5.25, pic_id: "dk.png", itemid:"", ing1:-5, ing2:0, ing3:-1, ing4:-3});


    catid =Category.insert({seqnumber:"106", name: "MemberShip", pic_id:"dk.png", quantity:0});
    MenuItem.insert({category: catid, seqnumber:'601', name: "N-P Lobby 2", price: 22.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'602', name: "N-P Lobby 5", price: 48.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'603', name: "N-P Lobby 10", price: 84.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'604', name: "Peak Lobby 2", price: 27.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'605', name: "Peak Lobby 5", price: 60.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'606', name: "Peak Lobby 10", price: 105.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'607', name: "N-P VIP 2", price: 35.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'608', name: "N-P VIP 5", price: 77.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'609', name: "N-P VIP 10", price: 134.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'610', name: "Peak VIP 2", price: 43.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'611', name: "Peak VIP 5", price: 96.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'612', name: "Peak VIP 10", price: 168.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'613', name: "(S-VIP) Lobby 10", price: 90.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'614', name: "(S-VIP) Lobby 35", price: 270.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'615', name: "(S-VIP) Lobby 75", price: 540.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'616', name: "(S-VIP) VIP 10", price: 140.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'617', name: "(S-VIP) VIP 35", price: 420.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'618', name: "(S-VIP) VIP 75", price: 840.00, pic_id: "dk.png", gst: 0.05, pst:0.07, printbypass:true});


    catid =Category.insert({seqnumber:"111", name: "Member Store Value", pic_id:"dk.png", quantity:0});
    MenuItem.insert({category: catid, seqnumber:'1100', name: "50", price: 50.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1101', name: "100", price: 100.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1102', name: "150", price: 150.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1103', name: "200", price: 200.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1104', name: "250", price: 250.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1105', name: "300", price: 300.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1106', name: "350", price: 350.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1107', name: "400", price: 400.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1108', name: "450", price: 450.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1109', name: "500", price: 500.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});
    MenuItem.insert({category: catid, seqnumber:'1110', name: "任意价格", price: 0.00, pic_id: "dk.png", itemid:"", ingred: "", quantity: 99999, gst: 0.00, pst:0.00, printbypass:true});



    
    
};
