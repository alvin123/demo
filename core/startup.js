if (Meteor.isServer) {
	
    Meteor.startup(function () {
        CloudControl.init({
            redis_port:'6379',      //optional, default is 6379
            redis_host:'localhost', //optional, default is localhost
            auth:false              //optional, default is false
        });
    });
    
}