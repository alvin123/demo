Template.mainlayout.events({
    'click .logout': function(event){
        event.preventDefault();
				Meteor.logout();
				Router.go('/');
    }
});

Template.home.events({


   'click #changePasswordBtn':function(event){
        event.preventDefault();
        $('#changePassword_modal_id').modal('show');
  }
});

Template.changePassword_modal.events({
    'submit #changePassword_form':function(event){
        event.preventDefault();
        var oldKey = Keykey.findOne({_id:"sammykey"}); 
        var backOfficePassword = $('#backOfficePassword').val();       
        var oldPassword = $('#oldPassword').val();
        var newPassword = $('#newPassword').val();
        var newPasswordCheck = $('#newPasswordCheck').val();
        console.log(backOfficePassword);

        if (backOfficePassword == oldKey.keykey){
        	if(newPassword==newPasswordCheck){
              Accounts.changePassword(oldPassword, newPassword,function(error){;
                 if (error) {
                   alert("Password update failed");
                 } else {
                   alert("Password update Success");
                 }
                 });
             }else{
             	   alert("newPassword does not match");
             }
             }else{
                 alert("Back Office Password Incorrect");
             }

            
       $('#changePassword_modal_id').modal('hide');
       $('#oldPassword').val("");
       $('#newPassword').val("");
       $('#newPasswordCheck').val("");
       $('#backOfficePassword').val("");

    }

});