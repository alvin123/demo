Template.membership.events({
	"change #updatemembertype":function(event){
        event.preventDefault();
        Session.set("updatemembertype", JSON.parse($('#updatemembertype').val()));
    },
	"submit #membersearch":function(event){
		event.preventDefault();
		// Session.set("memberupdatephone",null);
		$("#memberupdatephone").val("");
		var memberid = $("#memberupdateid").val();
		Session.set("memberlookup",Membership.findOne({"memberid":memberid}));
	},
	"submit #memberphonesearch":function(event){
		event.preventDefault();
		// Session.set("memberupdateid",null);
		$("#memberupdateid").val("");
		var memberphone = $("#memberupdatephone").val();
		Session.set("memberphonesearchresult",Membership.find({"memberphone":memberphone}).fetch());
		$("#phoneresult_modal_id").modal('show');
		//console.log(Session.get("memberphonesearchresult"));
		//var phoneresult = Session.get("memberphonesearchresult");
		//alert(phoneresult.length);
	},
	"submit #memberupdateform":function(event){
		event.preventDefault();
		var membertype = Session.get("updatemembertype");
		var key = $("#updatekey").val();
		var member = Session.get("memberlookup");
		var rcpt = $("#updatereceipt").val();
		Meteor.call("updatemember", member.memberid, membertype, key, rcpt, function(err,res){
			alert(res);
			/*

			if (res==true) {
				alert("充值成功");
			} else if (err) {
				alert(err);
			} else {
				alert("系统错误");
			}
			*/
		});
	},
	"submit #memberupdate2form":function(event){
		event.preventDefault();
		var membermoney = $("#updatemembermoney").val();
		var key = $("#update2key").val();
		var member = Session.get("memberlookup");
		var rcpt = $("#update2receipt").val();
		Meteor.call("update2member", member.memberid, membermoney, key, rcpt, function(err,res){
			alert(res);
			/*

			if (res==true) {
				alert("充值成功");
			} else if (err) {
				alert(err);
			} else {
				alert("系统错误");
			}
			*/
		});
	},

});

Template.membership.helpers({
	onemember:function(){
		// var memberid = Session.get("memberupdateid");
		// var memberphone = Session.get("memberupdatephone");
		var onemember = Session.get("memberlookup");
		// Membership.findOne({ $or: [{memberid:memberid},{memberphone:memberphone}] });
		return onemember;
	}
});

Template.phoneresult_modal.helpers({
	phoneresult:function(){
		var phoneresult = Session.get("memberphonesearchresult");
		return phoneresult;
	}
});
