Meteor.methods({
	updatemember:function(id,type,key,rcpt){
		var member = Membership.findOne({memberid:id});
		if (key=="0965" || key=="07784108" || key=="07784019" || key=="07784150" || key=="07784111" || key=="07780548" || key=="07780650"){

			//
			// var mem=Membership.findOne({_id:member._id});
			var mem=Membership.findOne({_id:member._id, 'membertype':{"$elemMatch":{'typename':type.type,'typepeak':type.peak}}});
			
			if(!mem) {
				//if member does not exist, push new member type
				Membership.update(member._id, {
					$push: {
						membertype: {
							typename: type.type,
							typepeak: type.peak,
							typesession: type.sessions,
							receipt: rcpt,
							purchasetime: new Date(),
							operator: key
						},
						buyhistory: {
							typename: type.type,
							typepeak: type.peak,
							typesession: type.sessions,
							receipt:rcpt,
							buydate: new Date(),
							operator: key
						}
					}
				});
				return "充值成功";
			} else {
				//if member already exist, increase existing membertype.typesession
				Membership.update({
						_id: member._id,
						'membertype': {"$elemMatch": {'typename': type.type, 'typepeak': type.peak}}
					},
					{$inc: {'membertype.$.typesession': type.sessions}});
				Membership.update({
						_id: member._id,
						'membertype': {"$elemMatch": {'typename': type.type, 'typepeak': type.peak}}
					},
					{$set: {'membertype.$.purchasetime': new Date()}});
				Membership.update(member._id, {
					$push: {
						buyhistory: {
							typename: type.type,
							typepeak: type.peak,
							typesession: type.sessions,
							receipt:rcpt,
							buydate: new Date(),
							operator: key
						}
					}
				});
				return "充值成功";
			}
			// return "充值成功";
		} else {
			return "管理员密码错误";
		}
	},
	update2member:function(id,money,key,rcpt){
		var member = Membership.findOne({memberid:id});
		if (key=="0965" || key=="07784108" || key=="07784019" || key=="07784150" || key=="07784111" || key=="07780548" || key=="07780650"){

			//
			// var mem=Membership.findOne({_id:member._id});
			var mem=Membership.findOne({_id:member._id, 'membertype':{"$elemMatch":{'typename':"Money",'typepeak':"Anytime"}}});
			
			if(!mem) {
				//if member does not exist, push new member type
				Membership.update(member._id, {
					$push: {
						membertype: {
							typename: "Money",
							typepeak: "Anytime",
							typesession: parseFloat(money),
							receipt: rcpt,
							purchasetime: new Date(),
							operator: key
						},
						buyhistory: {
							typename: "Money",
							typepeak: "Anytime",
							typesession: parseFloat(money),
							receipt:rcpt,
							buydate: new Date(),
							operator: key
						}
					}
				});
				return "充值成功";
			} else {

				

				
				//if member already exist, increase existing membertype.typesession
				Membership.update({
						_id: member._id,
						'membertype': {"$elemMatch": {'typename': "Money", 'typepeak': "Anytime"}}
					},
					{$inc: {'membertype.$.typesession': parseFloat(money)}});
				Membership.update({
						_id: member._id,
						'membertype': {"$elemMatch": {'typename': "Money", 'typepeak': "Anytime"}}
					},
					{$set: {'membertype.$.purchasetime': new Date()}});
				Membership.update(member._id, {
					$push: {
						buyhistory: {
							typename: "Money",
							typepeak: "Anytime",
							typesession: parseFloat(money),
							receipt:rcpt,
							buydate: new Date(),
							operator: key
						}
					}
				});
				return "充值成功";
				
			}
			// return "充值成功";
		} else {
			return "管理员密码错误";
		}
	},
	decrease_member_typesession:function(id,typename,typepeak){
		console.log(' '+id+' '+typename+' '+typepeak);
		var mem = Membership.findOne({
			_id: id,
			'membertype': {
				"$elemMatch": {
					'typename': typename,
					'typepeak': typepeak,
					typesession: {$gt: 0} //making sure the session > 0 otherwise not allowed to decrease further
				}
			}
		});
		if(!mem)
			throw new Meteor.Error( "此会员需要购买"+' '+typename+' '+typepeak);
		//if(mem.membertype.typesession<=0)
		//	throw new Meteor.Error( "无法操作: 此会员需要充值!");

		Membership.update({_id:id,'membertype':{"$elemMatch":{'typename':typename,'typepeak':typepeak}}},
						 {$inc: {'membertype.$.typesession': -1} });
		Membership.update({_id:id},{ $push: {"playhistory":{"typename":typename, "typepeak":typepeak, "playsession":1, "playdate": new Date()}}});
	},
	decrease_member_money:function(id,money){
		var mem = Membership.findOne({_id:id,"membertype":{"$elemMatch":{"typename":"Money","typepeak":"Anytime","typesession":{$gt:0}}}});
		if(!mem) {
			var msg = "此会员没有余额！";
			return msg;
		} else {
			var bill = parseFloat(money)*(-1);
			Membership.update({_id:id,'membertype':{"$elemMatch":{'typename':"Money",'typepeak':"Anytime"}}},
						 {$inc: {'membertype.$.typesession': bill} });
		    Membership.update({_id:id},{ $push: {"playhistory":{"typename":"Money", "typepeak":"Anytime", "playsession":money, "playdate": new Date()}}});
		    return "消费已记录于会员卡";
		}
	}
});

