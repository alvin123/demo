Template.cart.events({

    "click .editspecreq":function(event){
        event.preventDefault();
        // var tableselid = event.target.value;
        Session.set('specreq_modal_data',this.seqnumber);
        Session.set('specreqitem',this.specreq);
        $('#itemspecreq').val(Session.get('specreqitem'));
        $('#specreq_modal_id').modal('show');
    },
    "click .fenmianbtn":function(event){
        event.preventDefault();
        Session.set('fenmian_modal_data',this.seqnumber);
        Session.set('specreqitem',this.specreq);
        $('#fenmianitem').val(Session.get('specreqitem'));
        $('#fenmian_modal_id').modal('show');
    },
    "click .specreqbtn":function(event){
        event.preventDefault();
        var specreq = Session.get('specreqitem');
        var newitem = event.target.value;
        specreq += newitem;
        Session.set('specreqitem', specreq);
        $(event.target).addClass('specreq_selected');
    },
    "click .specreqbtn2":function(event){
        event.preventDefault();
        var specreq = Session.get('specreqitem');
        var newitem = event.target.value;
        specreq += newitem;
        Session.set('specreqitem', specreq);
        $(event.target).addClass('specreq_selected');
    },
    "click .voiditembtn":function(event){
        event.preventDefault();
        var tableid = Session.get('tableid');
        var itemid = Session.get('specreq_modal_data');
        Meteor.call('voidItem',tableid, itemid, "----已取消----", function(err,res){
            //
        });
    }
});

Template.mod2_modal.events({
    
    "click #mod2cancel":function(event){
        event.preventDefault();
        $('.mod2select').removeClass('mod2selected');
        $('#mod2item').val(Session.get('specreqitem'));
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);
    },
    "submit #mod2form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
        var specreqIce =  $("input:radio[name='ice']:checked").val();
        var specreqConfirm =specreqSugar +" " +  specreqIce;
        var specreqValue = Session.get('specreqitemsValue');
        var self = Session.get('thisItem');
        var addHot;
        
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = self.price.toFixed(2);
            var gstsub = (parseFloat(self.price) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(self.price) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:self.price,
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub = self.price.toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(self.price) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(self.price) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:self.price,
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        alert(self.name+'Added to cart');

          }); 
        

        $('#itemspecreq').val("");
        $(".mod2radiobtn").removeClass('active');
        $('#mod2_modal_id').modal('hide');

    },
});



Template.mod3_modal.events({
    "click .mod3select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod3_selected');
        var mod3item = $('#mod3item').val();
        var newmod3item = event.target.name + " ";
        mod3item += newmod3item;
        $('#mod3item').val(mod3item);        
        var mod3itemValue = Session.get('specreqitemsValue');
        var newmod3itemValue = parseFloat(event.target.value);
        var totalValue = parseFloat(mod3itemValue + newmod3itemValue);
        Session.set('specreqitemsValue', totalValue);
        Session.set('specreqitems', mod3item);
        


    },
    "click #mod3cancel":function(event){
        event.preventDefault();
        $('.mod3select').removeClass('mod3selected');
        $('#mod3item').val(Session.get('specreqitem'));
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);
    },
    "submit #mod3form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var defaultPrice = 0;
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
//        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
//        var specreqIce =  $("input:radio[name='ice']:checked").val();
        var specreqConfirm = specreq ;
        var specreqValue = (Session.get('specreqitemsValue') + defaultPrice);
        var self = Session.get('thisItem');
        var addHot;
        if($("#hot").is(':checked')){
           addHot = parseFloat(self.hot);
        }
        else{
            addHot = 0;
        }
   
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue) + parseFloat(addHot)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:(self.price + parseFloat(specreqValue)+parseFloat(addHot)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:(self.price + parseFloat(specreqValue) + addHot),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        

        $('#itemspecreq').val("");
        $(".mod3radiobtn").removeClass('active');
        $('#mod3_modal_id').modal('hide');
        $('#mod3item').val(Session.get('specreqitem'));
        
    },
});


Template.mod4_modal.events({
    "click .mod4select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod4_selected');
        var newmod4item = event.target.name + " ";
        $('#mod4item').val(newmod4item);        
        Session.set('specreqitems', newmod4item);



    },
    "click #mod4cancel":function(event){
        event.preventDefault();
        $('.mod4select').removeClass('mod4selected');
        $('#mod4item').val(Session.get('specreqitem'));
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);
    },
    "submit #mod4form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var defaultPrice = 0;
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
//        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
//        var specreqIce =  $("input:radio[name='ice']:checked").val();
//        var specreqConfirm = specreq ;
        var specreqValue = (Session.get('specreqitemsValue') + defaultPrice);
        var self = Session.get('thisItem');
        var addHot;
        if($("#hot").is(':checked')){
           addHot = parseFloat(self.hot);
        }
        else{
            addHot = 0;
        }
   
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue) + parseFloat(addHot)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:(specreq + self.name),
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:"",
                    price:(self.price + parseFloat(specreqValue)+parseFloat(addHot)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:(specreq + self.name),
                pic_id:self.pic_id,
                specreq:"",
                price:(self.price + parseFloat(specreqValue) + addHot),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        

        $('#itemspecreq').val("");
        $('#mod4_modal_id').modal('hide');
        $('#mod4item').val(Session.get('specreqitem'));
        
    },
});

Template.mod5_modal.events({
    "click .mod2select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod2_selected');
        var mod2item = $('#mod2item').val();
        var newmod2item = event.target.name + " ";
        mod2item += newmod2item;
        $('#mod2item').val(mod2item);        
        var mod2itemValue = Session.get('specreqitemsValue');
        var newmod2itemValue = parseFloat(event.target.value);
        var totalValue = parseFloat(mod2itemValue + newmod2itemValue);
        Session.set('specreqitemsValue', totalValue);
        Session.set('specreqitems', mod2item);
    },


    "click .specreq1": function(e){
        e.preventDefault();
        $('.specreq1').removeClass('specreq_selected');
        $(e.target).addClass('specreq_selected');
        var specreq1val = e.target.value;
        Session.set('specreq1val', specreq1val);
    },
    "click .specreq2": function(e){
        e.preventDefault();
        $('.specreq2').removeClass('specreq_selected');
        $(e.target).addClass('specreq_selected');
        var specreq2val = e.target.value + " ";
        Session.set('specreq2val', specreq2val);
    },
    "click .specreq3": function(e){
        e.preventDefault();
        $('.specreq3').removeClass('specreq_selected');
        $(e.target).addClass('specreq_selected');        
        var specreq3val = e.target.value + " ";
        Session.set('specreq3val', specreq3val);

    },

    "click .specreq4": function(e){
        e.preventDefault();
        $('.specreq4').removeClass('specreq_selected');
        $(e.target).addClass('specreq_selected');        
        var specreq4val = e.target.value + " ";
        Session.set('specreq4val', specreq4val);

    },
    


    "click #mod2cancel":function(event){
        event.preventDefault();
        $('.mod2select').removeClass('mod2selected');
        $('#mod2item').val(Session.get('specreqitem'));
        $('.specreq4').removeClass('specreq_selected');
        $('.specreq3').removeClass('specreq_selected');
        $('.specreq2').removeClass('specreq_selected');
        $('.specreq1').removeClass('specreq_selected');
        Session.set('specreqitems',"");
        Session.set('specreqitemsValue',0);

        Session.set('specreq4val', '');
        Session.set('specreq3val', '');
        Session.set('specreq2val', '');
        Session.set('specreq1val', '');

    },
    "submit #mod2form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var defaultPrice = 0;
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
        var specreq4val = Session.get('specreq4val');
        var specreq3val = Session.get('specreq3val');
        var specreq2val = Session.get('specreq2val');
        var specreq1val = Session.get('specreq1val');
        var specreqbox = $('#mod2item').val();

//        var specreqSugar =  $("input:radio[name='sugar']:checked").val();
//        var specreqIce =  $("input:radio[name='ice']:checked").val();
        var specreqConfirm = (specreq + specreq1val +  specreq3val +  specreq4val);
        var specreqValue = (Session.get('specreqitemsValue') + defaultPrice);
        var self = Session.get('thisItem');
        var addHot;

        if($("#hot").is(':checked')){
           addHot = parseFloat(self.hot);
        }
        else{
            addHot = 0;
        }
        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:(specreq2val + self.name ),
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:(self.price + parseFloat(specreqValue)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);

            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:(specreq2val + self.name ),
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:(self.price + parseFloat(specreqValue)),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass
            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        

        $('#itemspecreq').val("");
        $(".mod2radiobtn").removeClass('active');
        $('#mod5_modal_id').modal('hide');
        $('#mod2item').val(Session.get('specreqitem'));
        Session.set('specreq4val', '');        
        Session.set('specreq3val', '');
        Session.set('specreq2val', '');
        Session.set('specreq1val', '');

    },
});

Template.mod6_modal.events({
    "click .mod6select":function(event){
        event.preventDefault();
        $(event.target).addClass('mod6_selected');
        var mod6item = $('#mod6item').val();
        var newmod6item = event.target.name + " ";
        mod6item += newmod6item;
        $('#mod6item').val(mod6item);        

        var mod6itemValue = Session.get('specreqitemsValue');
        var newmod6itemValue = parseFloat(event.target.value);
        var totalValue = parseFloat(mod6itemValue + newmod6itemValue);
        Session.set('specreqitemsValue', totalValue);
        Session.set('specreqitems', mod6item);


    },

    "click .mod6radiobtn":function(event){
        event.preventDefault();
    },

    "click #mod6cancel":function(event){
        event.preventDefault();
        $('.mod6select').removeClass('mod6selected');
        $('#mod6item').val("");        
        Session.set('specreqitems',null);
        Session.set('specreqitemsValue',0);
    },
    "submit #mod6form": function (event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        var tableid = Session.get("tableid");
        var specreq = Session.get('specreqitems');
        var specreqSugar =  $("input:radio[name='sugar1']:checked").val();
        var specreqIce =  $("input:radio[name='ice1']:checked").val();
        var specreqConfirm = specreq + specreqSugar +" " +  specreqIce;
        var specreqValue = Session.get('specreqitemsValue');
        var self = Session.get('thisItem');
        var addHot;
        var catid = Session.get('catid');


        if($("#hot").is(':checked')){
           addHot = parseFloat(self.hot);
        }
        else{
            addHot = 0;
        }

        if(specreqSugar=="全糖"){
            var sugarprotion=1;
        }else if(specreqSugar=="多糖"){
            var sugarprotion=1.2;
        }else if(specreqSugar=="八分糖"){
            var sugarprotion=0.8;
        }else if(specreqSugar=="半糖"){
            var sugarprotion=0.5;
        }else if(specreqSugar=="三分糖"){
            var sugarprotion=0.3;
        }else if(specreqSugar=="无糖"){
            var sugarprotion=0;
        }


        var cartid = Meteor.call('findCart', tableid, function(err, res){
        if(!res){
            var sub = (self.price + parseFloat(specreqValue) + parseFloat(addHot)).toFixed(2);
            var gstsub = (parseFloat(sub) * parseFloat(self.gst)).toFixed(2);
            var pstsub = (parseFloat(sub) * parseFloat(self.pst)).toFixed(2);

            var total = (parseFloat(sub) + parseFloat(gstsub) + parseFloat(pstsub)).toFixed(2);

            Cart.insert({
                seqnumber: Meteor.myFunctions.orderInc("userid"),
                tableid:tableid,
                time:new Date(),
                alert:false,
                activated:true,
                paid:false,
                orders:[{
                    seqnumber: Meteor.myFunctions.itemInc("userid"),
                    // itemid:self._id,
                    name:self.name,
                    catid:self.catid,
                    printbypass:self.printbypass,
                    pic_id:self.pic_id,
                    specreq:specreqConfirm,
                    price:(self.price + parseFloat(specreqValue)+parseFloat(addHot)),
                    gst:gstsub,
                    pst:pstsub,
                    gstrate:self.gst,
                    pstrate:self.pst,
                    checked:true,
                    split_divider:1,
                    newitem:true,
                    printbypass:self.printbypass,
                    ing1:(((parseFloat(self.ing1))*(parseFloat(sugarprotion))).toFixed(2)),
                    ing2:self.ing2,
                    ing3:self.ing3,
                    ing4:self.ing4
                }],
                subtotal:sub,
                gsttotal:gstsub,
                psttotal:pstsub,
                total:total,
                // comments:[{}]
            });
            
        }else{
            var oldsub = res.subtotal;
            var oldgst = res.gsttotal;
            var oldpst = res.psttotal;
            var oldtotal = res.total;

            var tempsub =  (self.price + parseFloat(specreqValue)+parseFloat(addHot)).toFixed(2);
            var sub = (parseFloat(oldsub) + parseFloat(tempsub)).toFixed(2);
            var tempgst = (parseFloat(tempsub) * parseFloat(self.gst)).toFixed(2);
            var gstsub = (parseFloat(oldgst) + parseFloat(tempgst)).toFixed(2);
            var temppst = (parseFloat(tempsub) * parseFloat(self.pst)).toFixed(2);
            var pstsub = (parseFloat(oldpst) + parseFloat(temppst)).toFixed(2);

            var total = (parseFloat(tempsub) + parseFloat(tempgst) + parseFloat(temppst) + parseFloat(oldtotal)).toFixed(2);


            Cart.update(res._id,{ $push: { orders: {
                seqnumber:Meteor.myFunctions.itemInc("userid"),
                // itemid:self._id,
                name:self.name,
                catid:catid,
                pic_id:self.pic_id,
                specreq:specreqConfirm,
                price:(self.price + parseFloat(specreqValue) + addHot),
                gst:tempgst,
                pst:temppst,
                gstrate:self.gst,
                pstrate:self.pst,
                checked:true,
                split_divider:1,
                newitem:true,
                printbypass:self.printbypass,
                ing1:(((parseFloat(self.ing1))*(parseFloat(sugarprotion))).toFixed(2)),
                ing2:self.ing2,
                ing3:self.ing3,
                ing4:self.ing4,
                sug:sugarprotion

            }
            }});
            Cart.update(res._id,{$set:{subtotal: sub}});
            Cart.update(res._id,{$set:{gsttotal: gstsub}});
            Cart.update(res._id,{$set:{psttotal: pstsub}});
            Cart.update(res._id,{$set:{total: total}});
        };
        var table = Table.findOne({'name':tableid});
        Table.update(table._id,{$set:{inuse:true}});
        // $('#addtocart_confirm_modal_id').modal('show');
        // alert('已加入点单！');
        });
        

        $('#itemspecreq').val("");
        $(".mod6radiobtn").removeClass('active');
        $('#mod6_modal_id').modal('hide');
    }
});