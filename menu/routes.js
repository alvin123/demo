
Router.route('/tablesel', {
    waitOn: function () {
        // return one handle, a function, or an array
        // return IRLibLoader.load('//rawgithub.com/ashleydw/lightbox/master/dist/ekko-lightbox.js');
        return IRLibLoader.load('/js/lightbox.js');
    },

    action: function () {
        this.render('tablesel');
        this.layout('mainlayout');
    }
});


Router.route('/menu/table/:tableid', {
    action: function () {
        this.render('categories',{
            data: function () {
                return {
                    tableid:this.params.tableid
                };
            }
        });
        this.layout('menulayout', {
            data: {tableid:this.params.tableid}
        });
    }
});

Router.route('/menu/table/:tableid/menuitem/:seqnumber', {
    action: function () {
        Session.set('tableid',this.params.tableid);
        Session.set('search_menuitem_seqnumber',this.params.seqnumber);
        this.render('singleitem');
        this.layout('menulayout',{
            data: {tableid:this.params.tableid}
        });
    }
});


Router.route('/menu/table/:tableid/category/:categoryid', {
    action: function () {
        this.render('items',{
            data: function () {
                var prods= MenuItem.find({category: this.params.categoryid});
                return {
                    products:prods,
                    tableid:this.params.tableid
                };
            }
        });
        this.layout('menulayout',{
            data: {tableid:this.params.tableid}
        });

        //if(Meteor.isClient)
            Session.set("tableid",this.params.tableid);
    }
});


Router.route('/menu/table/:tableid/cart', {
    action: function () {
        Session.set("tableid",this.params.tableid);
        this.render('cart');
        this.layout('mainlayout',{
            data: {tableid:this.params.tableid}
        });

    }
});
