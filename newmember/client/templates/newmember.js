Template.newmember.helpers({
    member:function(){
        var members = Membership.find({newmember:true});
        return members;
    },
    categories: function () {
        return Category.find({name:"会员充值"});
    }

});

Template.newmember.events({
    "click .newmember":function(event){
        event.preventDefault();
        Session.set("mongomemberid", this._id);
        //alert(this._id);
        $('#signup_modal_id').modal('show');
    },
    "click .newmemberdel":function(event){
        event.preventDefault();
        Session.set("mongomemberid", this._id);
        //alert(this._id);
        $('#signupdel_modal_id').modal('show');
    }
});

Template.signup_modal.events({
    "change #membertype":function(event){
        event.preventDefault();
        Session.set("membertype", JSON.parse($('#membertype').val()));
    },
    "submit #signup_confirm_form": function(event){
        event.preventDefault();
        var mongomemberid = Session.get("mongomemberid");
        var memberid = $("#memberid").val();
        var key = $("#signup_password").val();
        var membertype = Session.get("membertype");
        var memberpassword = $("#memberpassword").val();
        var rcpt = $("#receiptnumber").val();

        Meteor.call('signupconfirmfinal', mongomemberid, memberid, membertype, memberpassword, key, rcpt, function(err,res){
            if (res==true) {
                alert("新会员已登记！");
            } else {
                alert(res);
            }
        });

        $('#signup_modal_id').modal('hide');
        $("#memberid").val("");
        $("#signup_password").val("");
        $("#membertype").val("");
        $("#memberpassword").val("");



    }
});

Template.signupdel_modal.events({
    "click #signupdel_confirm":function(event){
        event.preventDefault();
        var key = $("#signupdel_key").val();
        var memberid = Session.get("mongomemberid");
        Meteor.call("signupdel",memberid,key,function(err,res){
            alert(res);
        });
    }
});
