Meteor.methods({
	signupconfirmfinal:function(mongoid,id,type,password,key,rcpt){
		var member1 = Membership.findOne({memberid:id});
		if (member1){
			return "会员已存在";
		} else if (key=="0965" || key=="07784108" || key=="07784019" || key=="07784111" || key=="07784150" || key=="07780650" || key=="07780548"){
			var member2 = Membership.findOne({_id:mongoid});
			if (member2.memberid) {
				return "已有会员卡";
			} else {
				Membership.update({_id:mongoid},{ $set: {memberid:id, memberpassword:password, membertype:[], newmember:false}});
				Membership.update({_id:mongoid},{ $push: {membertype: {typename:type.type, typepeak:type.peak, typesession:type.sessions, receipt:rcpt, purchasetime:new Date(), operator:key}} });
				Membership.update({_id:mongoid},{ $push: {buyhistory: {typename:type.type, typepeak:type.peak, typesession:type.sessions, receipt:rcpt, buytime:new Date(), operator:key}} });
				console.log("member confirmed : " + member2.memberfirstname + " " + member2.memberlastname);
				return true;
			}
		} else {
			return "管理员密码错误";
		}
	},
	signupdel:function(mongoid,key){
		if (key=="0965" || key=="07784108" || key=="07784019" || key=="07784111" || key=="07784150" || key=="07780650" || key=="07780548"){
			var member = Membership.findOne({_id:mongoid});
			Membership.update({_id:mongoid},{ $set: {newmember:false}});
			return "已删除";
		} else {
			return "密码错误";
		}
	}
});