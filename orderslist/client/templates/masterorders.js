Template.registerHelper('equals', function (a, b) {
    return a === b;
});

Template.registerHelper('tillMomentHour', function(time){
    var now = moment();
    var a = now.diff(time)/3600000;
    return Math.floor(a);
    // return moment(time).fromNow();
});
Template.registerHelper('tillMomentMinute', function(time){
    var now = moment();
    var a = Math.floor(now.diff(time)/3600000);
    var b = now.diff(time)/60000 - a*60;
    return Math.floor(b);
    // return moment(time).fromNow();
});
Template.registerHelper('tillMoment', function(time){
    
    return moment(time).fromNow();
});

Template.registerHelper('formatTime', function(time){
    return moment(time).format('MM/DD/YYYY');
})

Template.orderslist.helpers({
    carts:function(){
        var carts= Cart.find({},{sort:{time:-1}});
        return carts;
    }
});


Template.orderslist.events({

    "click .orderdetails":function(event){
        event.preventDefault();
        // var tableselid = event.target.value;
        Session.set('masterorders_modal_data',this._id);
        $('#masterorders_modal_id').modal('show');
    }
});


Template.masterorders_modal.helpers({
    data:function(){
        return Session.get('masterorders_modal_data');
    },
    cartmodal:function(){
        var cartid = Session.get('masterorders_modal_data');
        return Cart.findOne({_id:cartid});
    }
});

