CloudControl package for remotely starting/stoping netbar computers

The package requires a running redis server!



API definitions

1.initialization - server side only

        Within Meteor.startup block, call following -
        CloudControl.init({
            port:string, default '6379', its the redis server port
            host:string, default 'localhost', its the redis server ip
            auth:boolean, default false. set true if you want only logged on user to operate
        })
                
        NOTE: all of these parameters are optional. They all have defaults




2.start/stop computers - server and client side
        CloudControl.start(computername)
        CloudControl.stop(computername)