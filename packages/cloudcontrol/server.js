CloudControl=(function(){
    // =========private fields=========
    var port=-1;
    var host=-1;
    var redisclient=undefined;
    var requireAuth=false;

    // =========private helper functions=========
    function start_stop(computername,what){
        var action='';
        var signal='';
        if(what=='s') {
            action = 'start';
            signal='UNLOCK|'+computername;
        }else if(what=='t') {
            action = 'stop';
            signal='LOCK|'+computername;
        }

        if(computername==undefined)
            throw new Meteor.Error('CloudControl - Can not '+action+' computer,computer name not provided');
        redisclient.publish("remote_computer_controll", signal);

    }

    function check(){
        if ((requireAuth&&Meteor.userId()==undefined))
            throw new Error('please login first');
        else if(redisclient==undefined)
            throw new Error('CloudControl is not initialized yet!');
        //TODO: add role authorization in the future
    }

    // =========main api definition=========
    return {
        /**
         * initialization
         * @param options{
         *      redis_port:redis server port
         *      redis_host:redis server ip
         *      auth:boolean, true if you want only logged on user to operate
         * }
         */
        init:function(options){
            if(options.redis_port==undefined)
                port='6379';
            else
                port=options.redis_port;

            if(options.redis_host==undefined)
                host='localhost';
            else
                host=options.redis_host;

            requireAuth=options.auth;

            var redis = Npm.require("redis");
            redisclient = redis.createClient(port,host,{});
        },
        start:function(computername){
            check();
            start_stop(computername,'s');

        },
        stop:function(computername){
            check();
            start_stop(computername,'t');
        }

    }
})();


//exposing to client api
Meteor.methods({
    'cloudcontrol.start':function(compid){
        CloudControl.start(compid);
    },
    'cloudcontrol.stop':function(compid){
        CloudControl.stop(compid);
    }
});

