Template.pos_tablelist.events({
    "click .tablelistbtn": function (event) {
        // Prevent default browser form submit
        event.preventDefault();
        var tablelistid = event.target.value;
        Session.set('cart', Cart.findOne({'tableid':tablelistid, "activated":true}));
        Session.set('tableid',tablelistid);
        Router.go('/posadmin/tablelist/'+tablelistid);
    },
    "click #combineorder":function(event){
        event.preventDefault();
        $("#combineorder_modal_id").modal("show");
    },
    "click #startall":function(event){
        event.preventDefault();
        if(!confirm("确定打开所有客户机吗?!"))
            return;
        var tabs=Table.find().fetch();
        tabs.forEach(function(t){
            CloudControl.start(t.name);
        });
    },
    "click #stopall":function(event){
        event.preventDefault();

        if(!confirm("确定关闭所有客户机吗?!"))
            return;

        var tabs=Table.find().fetch();
        tabs.forEach(function(t){
            CloudControl.stop(t.name);
        });    }
});

Template.pos_tablelist.helpers({
	table_check: function(table){
		var tablename = String(table);
		return Table.findOne({'name':tablename});
	}
});

Template.combineorder_modal.events({
    "click .cbtablelistbtn": function(event){
        event.preventDefault();
        $(event.target).addClass("cbtablelistbtn_selected");
        var tableid = event.target.value;
        var cart = Cart.findOne({"tableid":tableid});
        var cartfrom = Cart.findOne({"tableid":"cb1"});
    },
    "click #combinebtn": function(event){
        event.preventDefault();
    },
    "click #clearcbtable": function(event){
        event.preventDefault();
        $(".cbtablelistbtn").removeClass("cbtablelistbtn_selected");
    }
})