Template.signup.events({
    "submit #signupform":function(event){
        event.preventDefault();
        var memberfirstname = $('#memberfirstname').val();
        var memberlastname = $('#memberlastname').val();
        var memberphone = $('#memberphone').val();
        var memberemail = $('#memberemail').val();
        Meteor.call('signupconfirm', memberfirstname, memberlastname, memberphone, memberemail, function(err,res){
            if (res) {
                $('#signup_alert_id').modal('show');
                $('#memberfirstname').val("");
                $('#memberlastname').val("");
                $('#memberphone').val("");
                $('#memberemail').val("");
            } else {
                alert(err);
            }
        });
    }
});